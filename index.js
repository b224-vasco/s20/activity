let number = Number(prompt("Give me a number: "));
console.log("The number you provided is: " + number);

for(let i = number; i>=0; i-- ){

	
	if(i%10===0){
		console.log("The number is divisible by 10. Skipping the number");
		continue;
	}

	else if(i%5===0){
		console.log(i);
		continue;
	}

	else if(i<=50){
		console.log("Current value is 50. Terminating the loop.");
		break;
	}

	
};

let myString = "supercalifragilisticexpialidocious";
let conStorage = "";

console.log(myString);

for(let i =0; i<myString.length; i++){
	if(
		myString[i].toLowerCase() == "a" ||
		myString[i].toLowerCase() == "e" ||
		myString[i].toLowerCase() == "i" ||
		myString[i].toLowerCase() == "o" ||
		myString[i].toLowerCase() == "u"
		){
		continue;
	}
	
	else{
		conStorage+=myString[i];
	}
}
console.log(conStorage);
